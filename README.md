=================
Restaurants
=================

-----------------
SETUP project
-----------------

Install required system packages:

.. code-block:: bash

    $ sudo apt-get install python3-pip
    $ sudo apt-get install python-dev

Create www directory where project sites and environment dir

.. code-block:: bash

    $ mkdir /var/www && mkdir /var/envs && mkdir /var/envs/bin
    
Install virtualenvwrapper

.. code-block:: bash

    $ sudo pip3 install virtualenvwrapper
    $ sudo pip3 install --upgrade virtualenv
    
Add these to your bashrc virutualenvwrapper work

.. code-block:: bash

    export WORKON_HOME=/var/envs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export PROJECT_HOME=/var/www
    export VIRTUALENVWRAPPER_HOOK_DIR=/var/envs/bin
    source /usr/local/bin/virtualenvwrapper.sh
    
Create virtualenv

.. code-block:: bash

    $ cd /var/envs && mkvirtualenv --python=/usr/bin/python3 restaurants
    
Install requirements for a project.

.. code-block:: bash

    $ cd /var/www/restaurants && pip install -r requirements.txt

-----------------
RUN project
-----------------

RUN tests.

.. code-block:: bash

    $ cd /var/www/restaurants && python manage.py test

RUN server

.. code-block:: bash

    $ cd /var/www/restaurants && python manage.py runserver
