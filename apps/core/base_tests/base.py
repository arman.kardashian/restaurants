
from django.core.urlresolvers import reverse

from rest_framework.test import APITestCase


class BaseTestCase(APITestCase):
    """
    Base test case to provide more organized behavior.
    Extend all test cases from this one.
    """

    model = None
    lookup_field = None

    def _generate_url(self, name, pk=None):
        """
        Generate URL by reverse() method
        :param name:
        :param pk:
        :return: url
        """

        if pk is None:
            url = reverse('api:{0}-list'.format(name))
        else:
            url = reverse('api:{0}-detail'.format(name), args=[pk])

        return url

    def _get_pk_value(self):

        if self.lookup_field:
            pk = getattr(self.model.objects.first(), self.lookup_field)
        else:
            pk = self.model.objects.first().pk

        return pk
