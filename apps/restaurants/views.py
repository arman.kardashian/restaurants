
from rest_framework.viewsets import ModelViewSet

from apps.restaurants.models import Restaurant
from apps.restaurants.serializers import RestaurantSerializer
from apps.restaurants.filters import RestaurantFilter


class RestaurantViewSet(ModelViewSet):

    queryset = Restaurant.objects.all().order_by('name')
    serializer_class = RestaurantSerializer
    filter_class = RestaurantFilter
