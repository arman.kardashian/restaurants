
from apps.core.base_tests.base import BaseTestCase
from apps.restaurants.models import Restaurant


class RestaurantsBaseTestCase(BaseTestCase):

    def __init__(self, *args, **kwargs):

        self.model = Restaurant
        self.url = 'restaurants'
        self.patch_data = dict()
        self.invalid_data = list()
        self.invalid_patch_data = list()
        self.invalid_post_data = list()
        self.post_data = {
            'name': 'test_name',
            'opens_at': '10:00',
            'closes_at': '16:00'
        }

        super().__init__(*args, **kwargs)

    def setUp(self):
        super().setUp()
        self.__add_data_to_db()
        self.__generate_invalid_data()
        self.__generate_patch_data()
        self.__generate_invalid_patch_data()
        self.__generate_invalid_post_data()

    ###############################################
    # ############### Get Methods ############### #
    ###############################################

    def test_get_request_200(self):

        response = self.client.get(self._generate_url(self.url))
        self.assertEqual(response.status_code, 200)

    ###############################################
    # ############ Retrieve Methods ############# #
    ###############################################

    def test_retrieve_request_200(self):

        pk = self._get_pk_value()
        response = self.client.get(self._generate_url(self.url, pk))
        self.assertEqual(response.status_code, 200)

    def test_retrieve_request_404(self):

        not_existing_pk = 0
        response = self.client.get(self._generate_url(self.url, not_existing_pk))
        self.assertEqual(response.status_code, 404)

    ###############################################
    # ######### Patch Methods ########## #
    ###############################################

    def test_patch_request_200(self):

        pk = self._get_pk_value()
        response = self.client.patch(self._generate_url(self.url, pk), self.patch_data)
        self.assertEqual(response.status_code, 200)

    def test_patch_request_400(self):

        pk = self._get_pk_value()

        for data in self.invalid_patch_data:
            response = self.client.patch(self._generate_url(self.url, pk), data['main_data'])
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.content.decode('utf-8'), data['message'])

    def test_patch_request_404(self):

        not_existing_pk = 0
        response = self.client.patch(self._generate_url(self.url, not_existing_pk), self.patch_data)
        self.assertEqual(response.status_code, 404)

    ###############################################
    # ######### Post Methods ########## #
    ###############################################

    def test_post_request_201(self):
        response = self.client.post(self._generate_url(self.url), self.post_data)
        self.assertEqual(response.status_code, 201)

    def test_post_request_400(self):

        for data in self.invalid_post_data:
            response = self.client.post(self._generate_url(self.url), data['main_data'])
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.content.decode('utf-8'), data['message'])

    ###############################################
    # ######### Delete Methods ########## #
    ###############################################

    def test_delete_request_204(self):

        pk = self._get_pk_value()
        response = self.client.delete(self._generate_url(self.url, pk))
        self.assertEqual(response.status_code, 204)

    def test_delete_request_404(self):

        not_existing_pk = 0
        response = self.client.delete(self._generate_url(self.url, not_existing_pk))
        self.assertEqual(response.status_code, 404)

    def __add_data_to_db(self):

        self.model.objects.create(**self.post_data)

    def __generate_patch_data(self):
        self.patch_data['name'] = 'new_name'

    def __generate_invalid_data(self):

        self.invalid_data = [
            {'field': 'name', 'value': '', 'message': 'This field may not be blank.'},
            {'field': 'opens_at', 'value': 'XYZ', 'message': 'Time has wrong format. Use one of these formats instead: hh:mm[:ss[.uuuuuu]].'},
            {'field': 'closes_at', 'value': 'QWE', 'message': 'Time has wrong format. Use one of these formats instead: hh:mm[:ss[.uuuuuu]].'},
        ]

    def __generate_invalid_patch_data(self):

        for data in self.invalid_data:
            self.invalid_patch_data.append({
                'message': u'{"'+data['field']+'":["'+data['message']+'"]}',
                'main_data': {data['field']: data['value']}
            })

    def __generate_invalid_post_data(self):

        for data in self.invalid_data:

            new_data = {
                'message': u'{"'+data['field']+'":["'+data['message']+'"]}',
                'main_data': dict()
            }

            for supposed in self.post_data:

                if supposed == data['field']:
                    new_data['main_data'].update({data['field']: data['value']})
                else:
                    new_data['main_data'].update({supposed: self.post_data[supposed]})

            self.invalid_post_data.append(new_data)
