
import rest_framework_filters as filters

from apps.restaurants.models import Restaurant


class RestaurantFilter(filters.FilterSet):

    opens_at_lte = filters.TimeFilter(name="opens_at", lookup_expr='lte')
    opens_at_gte = filters.TimeFilter(name="opens_at", lookup_expr='gte')
    closes_at_lte = filters.TimeFilter(name="closes_at", lookup_expr='lte')
    closes_at_gte = filters.TimeFilter(name="closes_at", lookup_expr='gte')

    class Meta:
        model = Restaurant
        fields = {
            'name': ('exact', 'icontains', ),
        }
