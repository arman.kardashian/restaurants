
from rest_framework import serializers

from apps.restaurants.models import Restaurant


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):

    id = serializers.ReadOnlyField()
    url = serializers.HyperlinkedIdentityField(view_name='api:restaurants-detail')

    class Meta:
        model = Restaurant
        fields = '__all__'
